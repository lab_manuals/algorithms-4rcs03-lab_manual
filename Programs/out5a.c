/******************************************************************************
OUTPUT
SAMPLE 1
Enter the number of vertices : 4
Enter the adjacency matrix :
0 1 1 0
1 0 0 1
1 0 0 1
0 1 1 0
Enter the starting vertex : 1
Vertex 1 reachable
Vertex 2 reachable
Vertex 3 reachable
Vertex 4 reachable

SAMPLE 2
Enter the number of vertices : 4
Enter the adjacency matrix :
0 1 0 0
1 0 0 0
0 0 0 1
0 0 1 0
Enter the starting vertex : 1
Vertex 1 reachable
Vertex 2 reachable
Vertex 3 not reachable
Vertex 4 not reachable

SAMPLE 3
Enter the number of vertices : 4
Enter the adjacency matrix :
0 1 0 0
0 0 1 0
0 0 0 1
0 0 0 0
Enter the source vertex : 2
Vertex 1 not reachable
Vertex 2 not reachable
Vertex 3 reachable
Vertex 4 reachable

SAMPLE 4
Enter the number of vertices : 4
Enter the adjacency matrix :
0 1 0 0
0 0 1 0
0 0 0 1
1 0 0 0
Enter the source vertex : 2
Vertex 1 reachable
Vertex 2 reachable
Vertex 3 reachable
Vertex 4 reachable
******************************************************************************/
