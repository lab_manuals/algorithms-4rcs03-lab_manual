/*********************************************************
Enter the number of queens : 3
No solution exists for the given problem instance.


Enter the number of queens : 4

Solution #1

# Q # # 
# # # Q 
Q # # # 
# # Q # 


Solution #2

# # Q # 
Q # # # 
# # # Q 
# Q # # 

Number of solution for the given problem instance is : 2
*********************************************************/
