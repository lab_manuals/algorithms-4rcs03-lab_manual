/******************************************************************************
Sample 1:
Binomial Coefficients
Calculates the value of nCk
Enter the value of n : 4
Enter the value of k : 6
Invalid Input n should be larger than k!

Sample 2:
Binomial Coefficients
Calculates the value of nCk
Enter the value of n : 6
Enter the value of k : 2
The Binomial matrix is :
	0	1	2	3	4	5	6
================================================================
i=0	1
i=1	1	1
i=2	1	2	1
i=3	1	3	3	1
i=4	1	4	6	4	1
i=5	1	5	10	10	5	1
i=6	1	6	15	20	15	6	1

The value of 6C2 is 15
******************************************************************************/
